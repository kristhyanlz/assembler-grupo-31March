.data 
	array_1:	.word	1:3 
	array_2:	.word	1:3 
	ingEV1: .asciiz "\nIngrese un elemento del vector 1: "
	ingEV2: .asciiz "\nIngrese un elemento del vector 2: "
	prodEsca: .asciiz "\nEl producto escalar es: "
.text 
main: 
	li      $t0, 0
    la      $t1, array_1
	
#Llenamos el primer array
loop1:
    bge     $t0, 3, end_loop1
		
		#Imprimir el texto
		la $a0, ingEV1 	
		li $v0, 4 				
		syscall
		
		li $v0, 5
		syscall 
		add $t4, $v0, $zero
	
		sw      $t4, 0($t1) # set value
		add    $t1, $t1, 4 # increment pointer
		add    $t0, $t0, 1 # i++
		
		#Imprimir el número
		move $a0, $v0
		li $v0, 1
		syscall
    j      loop1
end_loop1:
    
    li      $t0, 0
    la      $t1, array_2
	
#Llenamos el segundo array
loop2:
    bge     $t0, 3, end_loop2
		
		#Imprimir el texto
		la $a0, ingEV2 	
		li $v0, 4 				
		syscall
		
		li $v0, 5
		syscall 
		add $t4, $v0, $zero
	
		sw      $t4, 0($t1) # set value
		add    $t1, $t1, 4 # increment pointer
		add    $t0, $t0, 1 # i++
		
		#Imprimir el número
		move $a0, $v0
		li $v0, 1
		syscall
    j      loop2
end_loop2:
    
    li      $t0, 0
    la      $t1, array_1
	la		$t5, array_2
	li		$t3, 0	#sumatoria

loop3:
    bge     $t0, 3, end_loop3
    lw      $t2, 0($t1)
	lw		$t6, 0($t5)
    add    $t1, $t1, 4
	add    $t5, $t5, 4

	#multiplicacion
	mul		$t7, $t2, $t6
	
	#sumatoria
	add $t3, $t3, $t7

    add    $t0, $t0, 1 # i++
    j      loop3
end_loop3:
	#Imprimir el texto
	la $a0, prodEsca 	
	li $v0, 4 				
	syscall
		
	#Imprimir sumatoria
	move $a0, $t3
	li $v0, 1
	syscall
	
    jr $ra