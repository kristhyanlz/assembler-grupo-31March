.data
str_ingresar:             .asciiz "Ingresar un numero: \n"
str_imprimirMult:         .asciiz "\nLos 5 primeros multiplos son: "


array1:		.word	0:5 #tam10, contiene 22222...
str_esp:	.asciiz " "

.text
main: 

# Ingresar texto por teclado
  la $a0, str_ingresar 
  li $v0, 4            
  syscall

# Lee el numero ingresado
  li $v0, 5  
  syscall
  add $t2, $v0, $zero
  add $t3, $v0, $zero

# Imprime el multiplo
  la $a0, str_imprimirMult
  li $v0, 4
  syscall


	li $t1, 0 #contador 
	la $t0, array1

	while:
	beq $t1, 5, endWhile
	sw $t2, 0($t0)
	
	addi $t0, $t0, 4
	add $t2, $t2, $t3
	addi $t1, $t1, 1
	
	j while

	endWhile:
	li $t1, 0
	la $t0, array1

	while2:
	beq $t1, 5, endWhile2
	lw $t2, 0($t0)

	move $a0, $t2
	li $v0, 1
	syscall
	la $a0, str_esp
	li $v0, 4
	syscall

	addi $t0, $t0, 4
	addi $t1, $t1, 1

	j while2
	endWhile2:
	
	li $v0, 10
	syscall
 
